#include <Keyboard.h>

int cols[] = {3, 2, 14, A2, 15, 8 , 16};
int rows[] = {A0, 4, 7, 9, 10, 5, 6, A3};
const int cols_len = 7;
const int rows_len = 8;

int keys[8][7] = {
    {194 , 195 , 196 , 0   , 0   , 212 , 0  },
    {198 , 199 , 200 , 201 , 178 , 203 , 0  },
    {204 , 205 , 33 , 34 , 94  , 35 , 0  },
    {36 , 44  , 40  , 41  , 47  , 37 , 0},
    {60 , 55  , 56  , 57  , 42  , 38 , 218},
    {61 , 52  , 53  , 54  , 45  , 58 , 0  },
    {62 , 49  , 50  , 51  , 43  , 59 , 216},
    {0   , 48  , 46  , 95  , 10  , 197 , 217}
};


void setup() 
{ 
  Serial.begin(9600);
  int i;
  for(i = 0; i < cols_len; i++)
  {
    pinMode(cols[i], OUTPUT);
    digitalWrite(cols[i], HIGH);
  }
  for(i = 0; i < rows_len; i++)
  {
    pinMode(rows[i], INPUT);
    digitalWrite(rows[i], HIGH); //pullup resistor enabled
  }
}

void loop()
{
  int result[8];  
  for(int i = 0; i < cols_len; i++)
  {
    
    digitalWrite(cols[i], LOW);
    for(int j = 0; j < rows_len; j++)
    {
      result[j] = digitalRead(rows[j]);
      if(result[j] == 0){
          Keyboard.write(keys[j][i]);
      } 
    } 
    digitalWrite(cols[i], HIGH);
    delay(20);
  }
  delay(20);
}
